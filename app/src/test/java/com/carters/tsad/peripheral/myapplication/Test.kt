package com.carters.tsad.peripheral.myapplication

import android.util.SparseIntArray
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.DataBinderMapperImpl
import com.carters.tsad.peripheral.myapplication.databinding.ViewItemBinding
import io.mockk.*
import java.lang.Error
import java.lang.Exception
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import kotlin.test.Test
import kotlin.test.assertEquals

class MyClassLoader : ClassLoader() {
    override fun loadClass(name: String?): Class<*> {
        return if (name == "com.carters.tsad.peripheral.myapplication.DataBinderMapperImpl") {
            val stream = MyClassLoader::class.java.classLoader.getResourceAsStream("DataBinderMapperImpl.class")
            val buffer = ByteArray(2000)
            val length = stream.read(buffer)
            defineClass(name, buffer, 0, length)
        } else parent.loadClass(name)
    }
}

class Test {

    @Test
    fun test() {
        val activity = mockk<AppCompatActivity> {
            every { setContentView(any<Int>()) } just runs
            every { layoutInflater } returns mockk()
        }

        val view = mockk<View>()
        val viewBinding = mockk<ViewItemBinding> {
            every { root } returns view
        }
        mockkStatic(DataBindingUtil::class)

        val field = DataBindingUtil::class.java.getDeclaredField("sMapper")
        field.isAccessible = true
//        val fieldModifiers = Field::class.java.getDeclaredField("modifiers")
//        fieldModifiers.isAccessible = true
//        fieldModifiers.setInt(field, field.modifiers and Modifier.FINAL.inv())

//        field.set(null, mockk<DataBinderMapperImpl>())

        val jclass = MyClassLoader().loadClass("com.carters.tsad.peripheral.myapplication.DataBinderMapperImpl")

//        every { SparseIntArray(1) } returns mockk {}
////        mockkConstructor(SparseIntArray::class) {
////            every { anyConstructed<SparseIntArray>().put(any(), any()) } just runs
////        }
        every { DataBindingUtil.inflate<ViewDataBinding>(any(), any(), any(), any()) } returns viewBinding

        val actualViewBinding = activity.viewBinding<ViewItemBinding>().value

        verify(exactly = 1) { activity.setContentView(view) }
        assertEquals(viewBinding, actualViewBinding)
    }
}