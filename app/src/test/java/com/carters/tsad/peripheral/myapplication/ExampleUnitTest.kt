package com.carters.tsad.peripheral.myapplication

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import kotlin.test.assertEquals

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @org.junit.Test
    fun addition_isCorrect() {
        org.junit.Assert.assertEquals(4, 2 + 2)


        val double: Double? = null
        org.junit.Assert.assertNotNull(double)

    }
}

class ExampleUnitTest2 {
    @kotlin.test.Test
    fun addition_isCorrect() {
        kotlin.test.assertEquals(4, 2 + 2)
        kotlin.test.assertEquals(4.0, 2 + 2)
        val double: Double? = null
        kotlin.test.assertNotNull(double)
        double.dec()
    }


    @kotlin.test.Test
    fun addition_isCorrecta() {

        val viewModel: MyViewModel = MyViewModel(ItemsInMemoryRepository())

        val stateHistory = mutableListOf<List<Item>>()

        runBlocking {
            viewModel.state.collect { stateHistory += it }
            viewModel.filterBySales()
        }

        assertEquals(listOf(Item("", 0)), stateHistory.last())
    }
}