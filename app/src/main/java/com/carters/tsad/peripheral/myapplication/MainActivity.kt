package com.carters.tsad.peripheral.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.*
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.carters.tsad.peripheral.myapplication.databinding.ActivityMainBinding
import com.carters.tsad.peripheral.myapplication.databinding.ViewItemBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlin.properties.Delegates.observable
import kotlin.reflect.KClass

class MainActivity : AppCompatActivity() {

    private val viewModel: MyViewModel by viewModels { viewModelProviderFactory }

    private val layout: ActivityMainBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val adapter = ItemsAdapter(layoutInflater)
        layout.list.adapter = adapter

        // I don't know of an easy way to data bind a list to an Adapter, because it is not a View.
        viewModel.state2.observe(this) { items -> adapter.list = items }

        viewModel.state.observe(lifecycleScope) { items -> adapter.list = items }
    }
}

class ItemsAdapter(private val inflater: LayoutInflater) : ListAdapter<Item, MyViewHolder>() {

    override fun create(group: ViewGroup) = MyViewHolder(inflater, group)

    override fun bind(holder: MyViewHolder, item: Item) = holder.bind(item)

}

class MyViewHolder(
        inflater: LayoutInflater,
        group: ViewGroup
) : BaseViewHolder<ViewItemBinding>(ViewItemBinding::class, inflater, group) {

    fun bind(data: Item) {
        layout.item = data
    }
}

class MyViewModel(
        private val items: Items
) : ViewModel() {

    val state = MutableStateFlow<List<Item>>(emptyList())

    fun filterBySales() {
        state.value = listOf()
    }

}

enum class ItemSort { BySales }

class FilterItems(
    private val items: Items,
    private val query: ItemsQuery
) {

    fun addSort(sort: ItemSort) {

    }

}

interface ItemsQuery {

}

interface Items {
    suspend fun getAll(): List<Item>
    suspend fun getAllBySalesDescending(): List<Item>
}

data class Item(val name: String, val salesCount: Int)

class ItemsInMemoryRepository : Items {
    private val items = mutableListOf<Item>()

    override suspend fun getAll() = items

    override suspend fun getAllBySalesDescending() = items.sortedByDescending { it.salesCount }

}

val itemsRepo = ItemsInMemoryRepository()

val viewModelProviderFactory = object : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) = when(modelClass) {
        MyViewModel::class.java -> MyViewModel(itemsRepo)
        else -> throw error("ViewModel $modelClass not known")
    } as T
}

inline fun <reified T : ViewDataBinding> AppCompatActivity.viewBinding(): Lazy<T> = lazy {
    val viewBinding = layoutInflater.inflateBinding<T>()
    setContentView(viewBinding.root)
    viewBinding
}

fun <T : ViewDataBinding> AppCompatActivity.viewBinding(@LayoutRes layout: Int): Lazy<T> = lazy {
    val viewBinding = layoutInflater.inflateBinding<T>(layout)
    setContentView(viewBinding.root)
    viewBinding
}

inline fun <reified T : ViewBinding> LayoutInflater.inflateBinding(
        viewGroup: ViewGroup? = null,
        attachToRoot: Boolean = false
): T {
    val inflate = T::class.java.getMethod("inflate", LayoutInflater::class.java)
    return inflate(null, this) as T
}

fun <T : ViewBinding> LayoutInflater.inflateBinding(
        @LayoutRes layout: Int,
        viewGroup: ViewGroup? = null,
        attachToRoot: Boolean = false
): T {
    return DataBindingUtil.inflate(this, layout, viewGroup, attachToRoot)
}

fun <T : ViewBinding> LayoutInflater.inflateBinding(
        bindingClass: KClass<T>,
        viewGroup: ViewGroup? = null,
        attachToRoot: Boolean = false
): T {
    val inflate = bindingClass.java.getMethod("inflate", LayoutInflater::class.java)
    return inflate(null, this) as T
}

fun <T> Flow<T>.observe(coroutineScope: LifecycleCoroutineScope, observer: (T) -> Unit) =
        coroutineScope.launchWhenCreated { collect { observer(it) } }

abstract class ListAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    var list: List<T> by observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }

    abstract fun create(group: ViewGroup): VH

    abstract fun bind(holder: VH, item: T)

    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = create(parent)

    final override fun onBindViewHolder(holder: VH, position: Int) = bind(holder, list[position])

    final override fun getItemCount() = list.size
}

open class BaseViewHolder<T : ViewBinding> private constructor(
        protected val layout: T
) : RecyclerView.ViewHolder(layout.root) {

    constructor(
            bindingClass: KClass<T>,
            inflater: LayoutInflater,
            group: ViewGroup
    ) : this(inflater.inflateBinding(bindingClass, group, false))
}
